// Fill out your copyright notice in the Description page of Project Settings.

#include "ComponentPawn.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "MyPawnMovementComponent.h"

// Sets default values
AComponentPawn::AComponentPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creates and attaches components to pawn. Static Mesh collision references can be found implementing collision detection in
	// the if statement. Setting up the static mesh, relative location, and world scale gives physical properties to the pawn.
	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;	SphereComponent->InitSphereRadius(40.0f);	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));
	UStaticMeshComponent* SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));	SphereMesh->SetupAttachment(RootComponent);	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshComponent(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if (SphereMeshComponent.Succeeded())
	{
		SphereMesh->SetStaticMesh(SphereMeshComponent.Object);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		SphereMesh->SetWorldScale3D(FVector(0.8f));
	}

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	TimmyMovementComponent = CreateDefaultSubobject<UMyPawnMovementComponent>(TEXT("CustomMovements"));
	TimmyMovementComponent->UpdatedComponent = RootComponent;

}

// Called when the game starts or when spawned
void AComponentPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AComponentPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AComponentPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Binds our axis to the lateral and side to side movements we made for Timmy.cpp
	InputComponent->BindAxis("Lateral", this, &AComponentPawn::Lateral);
	InputComponent->BindAxis("SidetoSide", this, &AComponentPawn::SidetoSide);
}

void AComponentPawn::Lateral(float Value)
{
	if (TimmyMovementComponent && (TimmyMovementComponent->UpdatedComponent == RootComponent))
	{
		TimmyMovementComponent->AddInputVector(GetActorForwardVector() * Value);
	}
}

void AComponentPawn::SidetoSide(float Value)
{
	if (TimmyMovementComponent && (TimmyMovementComponent->UpdatedComponent == RootComponent))
	{
		TimmyMovementComponent->AddInputVector(GetActorRightVector() * Value);
	}
}

UPawnMovementComponent* AComponentPawn::GetMovementComponent() const
{
	return TimmyMovementComponent;
}