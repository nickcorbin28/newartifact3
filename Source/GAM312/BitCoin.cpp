// Fill out your copyright notice in the Description page of Project Settings.

#include "BitCoin.h"
#include "DrawDebugHelpers.h"

// Sets default values
ABitCoin::ABitCoin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// The references here to static mesh collision implement collision detection through the creation of scene, static mesh, and box components.
	// Afterwards, we attach the components to the root of the "BitCoin" object. We then set the world scale to properly interact with other objects in-engine.
	BitCoinRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = BitCoinRoot;
	BitCoinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	BitCoinMesh->AttachToComponent(BitCoinRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	BitCoinBank = CreateDefaultSubobject<UBoxComponent>(TEXT("BitCoinBank"));
	BitCoinBank->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	BitCoinBank->AttachToComponent(BitCoinRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	BitCoinBank->OnComponentBeginOverlap.AddDynamic(this, &ABitCoin::OnOverlapBegin);
	BitCoinBank->OnComponentEndOverlap.AddDynamic(this, &ABitCoin::OnOverlapEnd);

}

// Called when the game starts or when spawned
void ABitCoin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABitCoin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Physics properties built into Unreal are used in these events to trigger a string of text to appear in the place of the object upon the player character
// overlapping the location of which "BitCoin" is located.
void ABitCoin::OnOverlapBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	DrawDebugString(GetWorld(), GetActorLocation(), "I Found a BitCoin!", nullptr, FColor::White, 5.0f, false);
}

void ABitCoin::OnOverlapEnd(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex)
{
	DrawDebugString(GetWorld(), GetActorLocation(), "My BitCoin is gone.", nullptr, FColor::White, 5.0f, false);
	Destroy();
}