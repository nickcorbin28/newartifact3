// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "AIAgentSmithController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_API AAIAgentSmithController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	// This method will take action when AgentSmith completes a move from one target point to the next
	void OnMoveCompleted(FAIRequestID, const FPathFollowingResult& Result) override;
	
private:
	UPROPERTY()
		TArray<AActor*> Waypoints;

	FTimerHandle TimerHandle;

	UFUNCTION()
	void GoToNextWaypoint();

	int CurrentWaypointIndex = 0;
	int WaypointIndexCount = 0;
};
