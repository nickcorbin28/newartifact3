// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraBoss.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraBoss::ACameraBoss()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraBoss::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame

// The Unreal engine�s built-in camera system libraries are being used to switch between two different camera views.
// Clicking the Camera Boss C++ class in the Unreal Engines "World Outliner" allows for dynamic swapping between different camera
// views that are included in the "World Outliner". Should a object without a camera be selected, you would simply see what is
// in front of the object.
void ACameraBoss::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const float TimeBetweenCameraChanges = 2.0f;
	const float SmoothBlendTime = 0.75f;
	CameraSwitchTime -= DeltaTime;
	if (CameraSwitchTime <= 0.0f)
	{ 
		CameraSwitchTime += TimeBetweenCameraChanges;

		// Finds the actor that handles control for the local player.
		{
			APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
			if (OurPlayerController)
			{
				if (CameraTwo && (OurPlayerController->GetViewTarget() == CameraOne))
				{
					//Blend smoothly to camera two.
					OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);
				}
				else if (CameraOne)
				{
					//Cut instantly to camera one.
					OurPlayerController->SetViewTarget(CameraOne);
				}
			}
		}
	}

}
