// Fill out your copyright notice in the Description page of Project Settings.

#include "Timmy.h"
#include "GameFramework/Pawn.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"


// Sets default values
ATimmy::ATimmy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATimmy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATimmy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATimmy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Called to receive player input from the "W" and "S" key being pressed, as specified in "Engine - Input"
	PlayerInputComponent->BindAxis("Lateral", this, &ATimmy::Lateral);

	// Called to receive player input from the "A" and "D" key being pressed, as specified in "Engine - Input"
	PlayerInputComponent->BindAxis("SidetoSide", this, &ATimmy::SidetoSide);

	// Called to receive player input from the "Left Shift" key being pressed, as specified in "Engine - Input"
	InputComponent->BindAction("Sprint", IE_Pressed, this, &ATimmy::SprintOn);
	InputComponent->BindAction("Sprint", IE_Released, this, &ATimmy::SprintOff);

}

// Notice the const variable "GetActorForwardVector" has an open parenthesis next to it, this is because a vector represents both a magnitude and a direction in an n -dimensional space 
// and has one real number component per dimension. We leave this open to "float Value", because the player input is what guides the magnitude and direction. This is also influenced
// by having the "Controller" variable included with the Value in the "if" statement.
void ATimmy::Lateral(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 9;
	{
		AddMovementInput(GetActorForwardVector(), Value / 9);
	}
}

// Same situation here, expect for the fact that we need to change up our const variable. A const variable's value is constant and tells the compiler to 
// prevent the programmer from modifying it. We need to change it to "GetActorRightVector" to have player input guide the magnitude and direction of the vector going to the right and left.
void ATimmy::SidetoSide(float Value)
{
	if (Controller && Value)
		if (IsSprinting)
			Value *= 9;
	{
		AddMovementInput(GetActorRightVector(), Value / 9);
	}
}

// Setting this variable to true lets the function know that we need to multiply our current movement value by 3, causing the character to speed up.
void ATimmy::SprintOn()
{
	IsSprinting = true;
}

// Setting this variable to false lets the function know that we need to divide our current movement value by 3, setting it back to the default value.
void ATimmy::SprintOff()
{
	IsSprinting = false;
}