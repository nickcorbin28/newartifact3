// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameModeBase.h"
#include "GAM312.h"

void AMyGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	ChangeMenuWidget(StartingWidgetClass);
}

// We need to define how we change between menus. We will need to remove whatever User Widget we have active from the viewport, if any. 
// Then we can create and a new User Widget and add it to the viewport. This code will create instances of any Widgets we provide and put them on the screen.
// It also removes them, so that only one is active at a time, although Unreal Engine can handle displaying and interacting with many Widgets at once.
void AMyGameModeBase::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}