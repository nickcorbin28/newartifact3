// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Timmy.generated.h"

UCLASS()
class GAM312_API ATimmy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATimmy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called to bind functionality for moving forward and backward
	void Lateral(float Value);

	// Called to bind functionality for moving to the left and right
	void SidetoSide(float Value);

	// Called to bind functionality for sprinting
	void SprintOn();
	void SprintOff();
	bool IsSprinting = false;
};