// Fill out your copyright notice in the Description page of Project Settings.

#include "AIAgentSmithController.h"

// Firstly, BeginPlay will store all of the Waypoints in the game.

void AAIAgentSmithController::BeginPlay() 
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);

	WaypointIndexCount = Waypoints.Num();

	GoToNextWaypoint();
}

// Secondly, OnMoveCompleted is a check to see if AgentSmith has reached the target point set in the virtual world. 
// A three second wait period is implmented before moving to the next target point.

void AAIAgentSmithController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AAIAgentSmithController::GoToNextWaypoint, 3.0f, false);
}

// Lastly, GoToNextWaypoint tells AgentSmith to move to the target point. 
// Should there be more than one target point on the map, AgentSmith must cycle between moving to and from the points infinitely.

void AAIAgentSmithController::GoToNextWaypoint()
{
	MoveToActor(Waypoints[CurrentWaypointIndex]);
	int Cycle = CurrentWaypointIndex % WaypointIndexCount;
	if (CurrentWaypointIndex >= WaypointIndexCount - 1)
	{
		CurrentWaypointIndex = 0;
	}
	else
	{
		CurrentWaypointIndex++;
	}
}
