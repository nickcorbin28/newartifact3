// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "BitCoin.generated.h"

UCLASS()
class GAM312_API ABitCoin : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABitCoin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		USceneComponent* BitCoinRoot;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BitCoinMesh;

	UPROPERTY(EditAnywhere)
		UShapeComponent* BitCoinBank;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

};